'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'user_game_biodata',
      [{
          uid: '9b03e274-fe1d-4837-82e4-a6df5c328d24',
          real_name: 'Sulekers',
          city: 'Belgium',
        },
        {
          uid: '2170d3a8-394d-4c3e-8952-6f3994226620',
          real_name: 'Andreas',
          city: 'Italy',
        },
        {
          uid: 'e6f2dd52-12c4-4684-bd42-225264eb7847',
          real_name: 'Jonson',
          city: 'Japan',
        },
      ], {}
    )
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('user_game_biodata', null, {})
  },
}