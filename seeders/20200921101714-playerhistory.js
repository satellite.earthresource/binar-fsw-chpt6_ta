'use strict'
const {
  v4: uuidv4
} = require('uuid')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'user_game_history',
      [{
          log_id: uuidv4(),
          user_id: '9b03e274-fe1d-4837-82e4-a6df5c328d24',
          level: 3,
          last_login: new Date(),
        },
        {
          log_id: uuidv4(),
          user_id: '2170d3a8-394d-4c3e-8952-6f3994226620',
          level: 2,
          last_login: new Date(),
        },
        {
          log_id: uuidv4(),
          user_id: 'e6f2dd52-12c4-4684-bd42-225264eb7847',
          level: 5,
          last_login: new Date(),
        },
        {
          log_id: uuidv4(),
          user_id: '2170d3a8-394d-4c3e-8952-6f3994226620',
          level: 5,
          last_login: new Date(),
        },
        {
          log_id: uuidv4(),
          user_id: 'e6f2dd52-12c4-4684-bd42-225264eb7847',
          level: 5,
          last_login: new Date(),
        },
        {
          log_id: uuidv4(),
          user_id: '9b03e274-fe1d-4837-82e4-a6df5c328d24',
          level: 6,
          last_login: new Date(),
        },
        {
          log_id: uuidv4(),
          user_id: '9b03e274-fe1d-4837-82e4-a6df5c328d24',
          level: 3,
          last_login: new Date(),
        },
      ], {}
    )
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('user_game_history', null, {})
  },
}