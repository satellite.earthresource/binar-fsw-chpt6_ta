const express = require('express')
const app = express()
const port = 5000
const bodyParser = require("body-parser");

//import model
const Sequelize = require('sequelize');
const sequelize = new Sequelize('chpt6_chlng', 'postgres', 'muscleup23', {
    host: 'localhost',
    dialect: 'postgres',
})

const PlayerModel = require('./models/player');
const Player = PlayerModel(sequelize, Sequelize);

app.use(express.static('public'))
app.use(bodyParser.urlencoded({
    extended: true
}))
app.set('view engine', 'ejs')

app.get('/', (req, res) => {
    res.render('home/index')
})

app.post('/login', (req, res) => {
    if (req.body.uname === 'binar' && req.body.psw === 'academy') {} else {
        res.redirect('/')
    }
    res.redirect('/dashboard')
})

app.get('/dashboard', (req, res) => {
    res.render('dashboard/index')
})

app.get('/players/', (req, res) => {
    // Player.findAll({
    //     include: ['PlayerBio']
    // }).then(players => res.json(players))
    sequelize.query('select * from user_game as u join user_game_biodata as b on u.id=b.uid join user_game_history as h on u.id=h.user_id').then(players => res.json(players));
})

app.post('/players/', (req, res) => {
    console.log(req.body.id)
    sequelize.query("insert into user_game (id, username, password) values('" + req.body.id + "', '" + req.body.username + "', '" + req.body.password + "')").then(players => res.json(players));
})

app.put('/players/:uid', (req, res) => {
    console.log(req.body.id)
    sequelize.query("update user_game set username='" + req.body.username + "', password='" + req.body.password + "' where id='" + req.body.id + "'").then(players => res.json(players));
})

app.delete('/players/:uid', (req, res) => {
    console.log(req.params.uid)
    sequelize.query("delete from user_game where id='" + req.params.uid + "'").then(players => res.json(players));
})


app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})