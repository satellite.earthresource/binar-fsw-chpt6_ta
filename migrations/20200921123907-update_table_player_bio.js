'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.addColumn(
        'user_game_biodata',
        'real_name', {
          type: Sequelize.DataTypes.STRING,
        }, {
          transaction
        }
      );
      await queryInterface.removeColumn('user_game_biodata', 'first_name', {
        transaction
      });
      await queryInterface.removeColumn('user_game_biodata', 'last_name', {
        transaction
      });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.addColumn(
        'user_game_biodata',
        'first_name', {
          type: Sequelize.DataTypes.STRING,
        }, {
          transaction
        }
      );
      await queryInterface.addColumn(
        'user_game_biodata',
        'last_name', {
          type: Sequelize.DataTypes.STRING,
        }, {
          transaction
        }
      );
      await queryInterface.removeColumn('user_game_biodata', 'real_name', {
        transaction
      });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  }
};